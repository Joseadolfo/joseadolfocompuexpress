var app = app || {};

app.note = (function () {
   
    /* var valuename_localstr = localStorage.getItem(keyname_localstr);
    console.log(valuename_localstr); */

    /* setTimeout(function () {
        console.log(keyname_localstr);
        console.log(valuename_localstr);

    }, 10000); */

/* 
    var keyname_localstr = $("#note_name").attr('id');//----------------------------atributo llave local storage de campo "name"
    var valuename_localstr = $("#note_name").textbox('getValue');//-----------------atributo valor local storage de campo "name"
    var keydescription_localstr = $("#note_description").attr('id');//--------------atributo llave local storage de campo "description"
    var valuedescription_localstr = $("#note_description").textbox('getValue');//---atributo valor local storage de campo "description" */

    //var keyname_localstr;//----------------atributo llave local storage de campo "name"
    //var objname_localstr = $("#note_name");//-------------------------atributo valor local storage de campo "name"
    //var valuename_localstr;
    /* var keydescription_localstr = $("#note_description").attr('id');//--atributo llave local storage de campo "description"
    var objdescription_localstr = $("#note_description");//-----------atributo valor local storage de campo "description"
    var valuedescription_localstr; */

    let route = "";
    let dg = $("#note_dg");
    let dlg = $("#note_dlg");
    let fm = $("#note_fm");
    let txt_name = $("#note_name");
    let txt_description = $("#note_description");
    let btn_save;
    let btn_cancel;
    let init = function () {
        dg.datagrid({
            title: "Note",
            url: "note/get",
            remoteFilter: true,
            toolbar: [{
                text: TEXT_ADD,
                iconCls: ICON_ADD,
                handler: function () {
                    insert();
                }
            }, "-", {
                text: TEXT_EDIT,
                iconCls: ICON_EDIT,
                handler: function () {
                    update();
                }
            }, "-", {
                text: TEXT_DELETE,
                iconCls: ICON_DELETE,
                handler: function () {
                    erase();
                }
            }],
            columns: [[
                {
                    field: "name",
                    title: "Name",
                    width: 100,
                },
                {
                    field: "description",
                    title: "Description",
                    width: 100,
                },
            ]],
            onDblClickRow: function () {
                update();
            }
        });

        dg.datagrid("enableFilter", [{}]);

        dlg.dialog({
            buttons: [{
                id: "note_cancel",
                text: TEXT_CANCEL,
                handler: function () {
                    dlg.dialog("close");
                }
            }, {
                id: "note_save",
                text: TEXT_SAVE,
                handler: function () {
                    save();
                }
            }],
            onOpen: function () {
                dlg.dialog("center");
                dlg.parent().animateCss(DIALOG_OPEN_ANIMATION);
                btn_save = $("#note_save");
                btn_cancel = $("#note_cancel");
                btn_save.addClass("md-btn");
                btn_cancel.addClass("md-btn-plain");
            }
        });

        txt_name.textbox({
            width: "100%",
            label: "Name",
            labelPosition: "top",
            required: true
        });

        txt_description.textbox({
            width: "100%",
            label: "Description",
            labelPosition: "top",
            required: false
        });

    };

    function insert() {
        //fm.form("clear");
        
        //asignacion de valores para primer campo
        var keyname_localstr = $("#note_name").attr('id');
        valuename_localstr = localStorage.getItem(keyname_localstr);
        var valuename_localstr = $("#note_name").textbox('getValue');
        
        //asignacion de valores para segundo campo
        var keydescription_localstr = $("#note_description").attr('id');
        valuedescription_localstr = localStorage.getItem(keydescription_localstr);
        var keydescription_localstr = $("#note_description").textbox('getValue');


        var timerlocalstr = setInterval(function () {//-------------------------------------------------------------bucle activado al iniciar dialogo

            //iteracion de asignacion localstorage primer campo
            var keyname_localstr = $("#note_name").attr('id');
            var valuename_localstr = $("#note_name").textbox('getValue');
            localStorage.setItem(keyname_localstr,valuename_localstr);
                        
            //iteracion de asignacion localstorage segundo campo
            var keydescription_localstr = $("#note_description").attr('id');
            var valuedescription_localstr = $("#note_description").textbox('getValue');
            localStorage.setItem(keydescription_localstr,valuedescription_localstr);
            
            //localStorage.setItem(keyname_localstr, valuename_localstr.textbox('getValue'));//-----------------------asignacion a local storage (llave,valor) de campo name
            //localStorage.setItem(keydescription_localstr, valuedescription_localstr.textbox('getValue'));//---------asignacion a local storage (llave,valor) de campo description
            
            //console.log(JSON.parse(localStorage.getItem(keyname_localstr)));

            /* var ObjetoJSONlocalestr = JSON.parse(localStorage.getItem(keyname_localstr))
            console.log(ObjetoJSONlocalestr); */

            document.getElementById("note_cancel").addEventListener("click", parar);//------------------------------listener de boton cancel para la accion clic
            document.getElementById("note_save").addEventListener("click", parar);//--------------------------------listener de boton save para la accion clic
        }, 1000);
        function parar() {//----------------------------------------------------------------------------------------funcion parar
            console.log("se cerro el dialogo = cancel");
            clearInterval(timerlocalstr);//-------------------------------------------------------------------------terminar ciclo
        }

        route = "note/add";
        dlg.dialog("open").dialog("setTitle", "Nuevo");
    }

    function update() {
        let row = dg.datagrid("getSelected");
        if (row) {
            dlg.dialog("open").dialog("setTitle", "Editar");
            fm.form("load", row);
            route = "note/edit/" + row.id;
        } else {
            show_info("Seleccione un registro.");
        }
    }

    function save() {
        fm.form("submit", {
            url: route,
            onSubmit: function () {
                let valid = fm.form("validate");
                if (valid) {
                    disable_buttons();
                }
                return valid;
            },
            success: function (result) {
                try {
                    result = JSON.parse(result);
                    if (result.success) {
                        dlg.dialog("close");
                        dg.datagrid("reload");
                        show_info(result.msg);
                    } else {
                        show_error(result.msg);
                    }
                    enable_buttons();
                } catch (e) {
                    show_error(e);
                    enable_buttons();
                }
            }
        });
    }

    function erase() {
        let row = dg.datagrid("getSelected");
        if (row) {
            $.messager.confirm("Confirmar", TEXT_CONFIRM, function (r) {
                if (r) {
                    $.post("note/delete/" + row.id)
                        .done(function (result) {
                            if (result.success) {
                                dg.datagrid("reload");
                                show_info(result.msg);
                            } else {
                                show_error(result.msg);
                            }
                        })
                        .fail(function (xhr, status, error) {
                            show_error(error);
                        });
                }
            });
        } else {
            show_info("Seleccione un registro");
        }
    }

    function disable_buttons() {
        btn_cancel.linkbutton("disable");
        btn_save.linkbutton("disable");
        btn_save.linkbutton({ text: "Guardando..." });
        dlg.dialog("showMask", "Procesando...");
    }

    function enable_buttons() {
        btn_cancel.linkbutton("enable");
        btn_save.linkbutton("enable");
        btn_save.linkbutton({ text: TEXT_SAVE });
        dlg.dialog("hideMask");
    }

    return {
        init: init
    };
})();
app.note.init();
